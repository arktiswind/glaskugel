// ==UserScript==
// @name         Glaskugel
// @namespace    https://gitlab.com/arktiswind/glaskugel/
// @version      0.2
// @description  try to take over RN!
// @author       Arktiswind
// @match        <$URL$>
// @updateURL    https://gitlab.com/arktiswind/glaskugel/-/blob/main/glaskugel.js
// @downloadURL  https://gitlab.com/arktiswind/glaskugel/-/blob/main/glaskugel.js
// @resource     IMPORTED_CSS https://gitlab.com/arktiswind/glaskugel/-/blob/4bb98339618d293f067a84593f57aa499780fba2/glaskugel.css
// @grant        GM_getResourceText
// @grant        GM_addStyle
// @grant        GM.xmlHttpRequest
// ==/UserScript==

(function(open) {
    'use strict';

    var harborWarehouseList = [];

    console.log('hello');

    XMLHttpRequest.prototype.wrappedSend = XMLHttpRequest.prototype.send;

    Object.filterTime = (obj, time) =>
        Object.keys(obj)
            .filter(key => key < Date.now() - (time * 1000))
            .reduce((res, key) => (res[key] = obj[key], res), {});

    // Override the existing setRequestHeader function so that it stores the headers
    XMLHttpRequest.prototype.send = function (value) {
        // Call the wrappedSetRequestHeader function first
        // so we get exceptions if we are in an erronous state etc.
        this.wrappedSend(value);
        this.payload = value;
    }

    XMLHttpRequest.prototype.open = function (method, url, async, user, pass) {

        this.addEventListener("readystatechange", function () {
            // get all warehouses on RN load
            if (url.includes("WarehouseInterface") && this.responseText != "") {
                var data = JSON.parse(this.responseText);
                console.log(data.Infos);
                harborWarehouseList.push(...data.Body);
                console.log(harborWarehouseList);
            }
            // get all harbors on RN load
            if (url.includes("HarborInterface") && this.responseText != "") {
                var data = JSON.parse(this.responseText);
                harborWarehouseList.push(...data.Body);
                console.log(harborWarehouseList);
            }

            if (url.includes("getBestRoutesForResource") && this.responseText != "") {
                console.log('juhu');
                var data = JSON.parse(this.responseText);
                var wares = data.Body;
                var result = [];
                Object.entries(wares).forEach(ware => {
                    Object.entries(ware.Buyers).forEach(buyer => {
                        var fWarehouseList = this.harborWarehouseList.filter(warehouse => warehouse.ID === buyer.ID);
                        console.log(fWarehouseList);
                    })
                    // this.results.push(
                    //     {
                    //         warehouse: warehouse.ID
                    //     }
                    // )
                    

                    
                });

            }

        }, false);

        open.call(this, method, url, async, user, pass);
    }
})(XMLHttpRequest.prototype.open);