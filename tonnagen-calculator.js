// ==UserScript==
// @name         Tonnagen - Calculator
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        <$URL$>
// @grant        GM.xmlHttpRequest
// ==/UserScript==

(function (open) {
    var isES = false;
    var showLM = true;
    var showTown = true;
    var pastData = {};
    var lastcalc = 0;
    var vainfo = false;

    console.log("test");
    $('head').append('<style>#datatable tr td, #datatable tr th { border:1px solid;border-collapse: collapse;padding: 4px 8px;text-align:right;width: 100px; }'
        + '#datatable { padding: 20px; }'
        + '#datatable .table2 tr td, #datatable .table2 tr th { width: 120px; }'
        + '</style')


    $('body').append('<div class="toggle-va" style="position: absolute;top: 10px;left: 546px;background: #425473;color:#E4E7ED;width: 80px;height: 30px;text-align: center;line-height: 30px;font-family: sans-serif;cursor: pointer;">VA Info</div>')
    var dataTable = $('<div id="datatable" style="max-height: 800px;overflow:scroll;position:absolute;top:150px;left:50%; width:1300px;margin-left:-650px;background:#fff;display:none;min-height:100px;font-family: sans-serif;"></div>')
    $('body').append(dataTable);
    $('.toggle-va').on('click', function () {

        vainfo = !vainfo;

        $('#datatable').toggle()
    });

    var enc = new TextDecoder("utf-8");

    var ResourceList = {
        1: "Kohle",
        2: "Getreide",
        3: "Eisen",
        4: "Holz",
        5: "Bretter",
        6: "Eisenerz",
        7: "Baumwolle",
        8: "Stoffe",
        9: "Rinder",
        10: "Fleisch",
        11: "Garn",
        12: "Eisenwaren",
        13: "Papier",
        14: "Leder",
        15: "Backwaren",
        16: "Mehl",
        17: "Kupfererz",
        18: "Quarz",
        19: "Kupfer",
        20: "Stahl",
        21: "Schuhe",
        22: "Glas",
        23: "Drähte",
        24: "Rohre",
        25: "Verpackungen",
        26: "Fenster",
        27: "Bleche",
        28: "Silizium",
        29: "Burger",
        30: "Erdöl",
        31: "Lampen",
        32: "Chemiekalien",
        33: "Kleidung",
        34: "Edelstahl",
        35: "Bauxit",
        36: "Maschinen",
        37: "Kunststoffe",
        38: "Aluminium",
        39: "Keramik",
        40: "Stahlträger",
        41: "Benzin",
        42: "Autos",
        43: "Töpfe",
        44: "Elektronik",
        45: "Enten",
        46: "Bälle",
        47: "Wannen",
        48: "Pharma",
        49: "Paxe",
        50: "Geld",
    }

    function getTownHash(sinceLevels) {
        var hash = 100000;
        for (var level in sinceLevels) {
            hash = hash / sinceLevels[level]
        }
        return hash;
    }

    function getSecondsSinceCalc(data) {
        return data.Body.LastConsumption * -1;
    }

    function getSeconds() {
        var d = new Date();
        var n = (d.getMinutes() * 60) + d.getSeconds();

        return n;

    }

    XMLHttpRequest.prototype.wrappedSend = XMLHttpRequest.prototype.send;


    Object.filterTime = (obj, time) =>
        Object.keys(obj)
            .filter(key => key < Date.now() - (time * 1000))
            .reduce((res, key) => (res[key] = obj[key], res), {});

    // Override the existing setRequestHeader function so that it stores the headers
    XMLHttpRequest.prototype.send = function (value) {
        // Call the wrappedSetRequestHeader function first
        // so we get exceptions if we are in an erronous state etc.
        this.wrappedSend(value);


        this.payload = value;

    }

    XMLHttpRequest.prototype.open = function (method, url, async, user, pass) {

        this.addEventListener("readystatechange", function () {

            if (url.includes("getTownDetails") && this.responseText != "" && showTown) {
                var data = JSON.parse(this.responseText);

                var secondsCalc = getSecondsSinceCalc(data);
                console.log(secondsCalc);
                var mSinceCalc = Math.floor(secondsCalc / 60)
                var sSinceCalc = secondsCalc % 60;

                var mToCalc = Math.floor((900 - secondsCalc) / 60)
                var sToCalc = (900 - secondsCalc) % 60;

                dataTable = $('#datatable').replaceWith('<div id="datatable" style="position:absolute;top:150px;left:50%; width:1300px;margin-left:-650px;background:#fff;' + (!vainfo ? 'display:none;' : '') + 'min-height:100px;font-family: sans-serif;"><div style="margin:0 0 10px 0;">Level: ' + data.Body.Level + '</div><div style="margin:0 0 20px 0;">Zeit seit Abzug: ' + mSinceCalc.toString().padStart(2, '0') + ':' + sSinceCalc.toString().padStart(2, '0') + ' | Zeit bis Abzug: ' + mToCalc.toString().padStart(2, '0') + ':' + sToCalc.toString().padStart(2, '0') + '</div></div>')
                var innerTable = $('<table class="table1" style="margin:0 0 20px 0;border:1px solid;border-collapse: collapse;border-spacing: 5px;"><tr><th>Ware</th><th>Menge</th><th>Abzug</th><th>grün bei</th><th>Fehlmenge</th><th>to/min</th><th>Menge bis VA</th><th>Fehlmenge bis VA</th></tr></table')
                var innerTable2 = $('<table class="table2" style="border:1px solid;border-collapse: collapse;border-spacing: 5px;"><tr><th>Ware</th><th>Schnitt 1min</th><th>Fehlmenge VA 1min</th><th>Schnitt 3min</th><th>Fehlmenge VA 3min</th><th>Schnitt 5min</th><th>Fehlmenge VA 5min</th></tr></table')

                var data60, data180, data300;
                var prioList = data.Body.Priorities;
                //console.log(JSON.parse(enc.decode(this.payload)).parameters);
                var time = Date.now();
                var townHash = JSON.parse(enc.decode(this.payload)).parameters[0];


                if (lastcalc > secondsCalc) {
                    pastData = {};
                }

                if (typeof pastData[townHash] != 'undefined') {
                    /************************* 60 SEK ********************************/
                    var data60List = Object.filterTime(pastData[townHash], 60);

                    if (Object.keys(data60List).length > 0) {
                        var data60keys = Object.keys(data60List);
                        var key60 = data60keys[data60keys.length - 1];
                        data60 = data60List[key60];

                    } else {
                        var data60keys = Object.keys(pastData[townHash]);
                        var key60 = data60keys[0];
                        data60 = pastData[townHash][key60];

                    }

                    /************************* 180 SEK ********************************/
                    var data180List = Object.filterTime(pastData[townHash], 180);

                    if (Object.keys(data180List).length > 0) {
                        var data180keys = Object.keys(data180List);
                        var key180 = data180keys[data180keys.length - 1];
                        data180 = data180List[key180];

                    } else {
                        var data180keys = Object.keys(pastData[townHash]);
                        var key180 = data180keys[0];
                        data180 = pastData[townHash][key180];

                    }

                    /************************* 300 SEK ********************************/
                    var data300List = Object.filterTime(pastData[townHash], 300);

                    if (Object.keys(data300List).length > 0) {
                        var data300keys = Object.keys(data300List);
                        var key300 = data300keys[data300keys.length - 1];
                        data300 = data300List[key300];

                    } else {
                        var data300keys = Object.keys(pastData[townHash]);
                        var key300 = data300keys[0];
                        data300 = pastData[townHash][key300];

                    }


                }
                for (var key in prioList) {
                    var value = prioList[key];
                    if (value == 1 || value == 0) {
                        var storage = data.Body.StoragesInfo.Storages.filter(stor => stor.ResourceId == key)[0]
                        var limit = storage.Limit;
                        var amount = storage.Amount;
                        var needed = limit * 0.67;
                        var consumption = storage.ConsumptionAmount;
                        var addPercent = 1 / (1 - consumption);
                        if (key == 49 || isES) {
                            var greenAmount = limit;
                        } else {
                            var greenAmount = addPercent * needed;
                        }
                        //var greenAmount= addPercent*needed; //Für Stadtaufstieg
                        //var greenAmount= limit; // Für ES
                        var tons = amount - storage.LastAmount;
                        var tonsMinute = (tons / secondsCalc) * 60;
                        var endTon = amount + ((900 - secondsCalc) * (tons / secondsCalc))
                        var netTons = ((tonsMinute * 15) - (greenAmount * consumption))
                        var netTonsCur = ((tonsMinute * 15) - (amount * consumption))
                        var netTonsMinute = (netTons / 900) * 60;
                        var netTonsMinuteCur = (netTonsCur / 900) * 60;
                        var timetoGreen = 0.0;
                        if (endTon >= greenAmount) {
                            timetoGreen = (greenAmount - amount) / tonsMinute
                        } else {
                            timetoGreen = (greenAmount - amount) / netTonsMinute
                        }



                        innerTable.append('<tr><td>' + ResourceList[key] + ' ' + (data.Body.LowerConsumptionResources[0] == key ? '(B)' : '') + '</td><td>' + new Intl.NumberFormat('de-DE').format(amount) + '</td><td>' + new Intl.NumberFormat('de-DE').format((consumption * 100).toFixed(2)) + '%</td><td>' + new Intl.NumberFormat('de-DE').format(Math.round(greenAmount)) + '</td><td>' + new Intl.NumberFormat('de-DE').format(Math.round(greenAmount - amount)) + '</td><td>~' + new Intl.NumberFormat('de-DE').format(Math.round(tonsMinute)) + '</td><td>~' + new Intl.NumberFormat('de-DE').format(Math.round(endTon)) + '</td><td ' + (Math.round(greenAmount - endTon) < 0 ? 'style="background:#B7D76A"' : '') + '>~ ' + new Intl.NumberFormat('de-DE').format(Math.round(greenAmount - endTon)) + '</td></tr>')
                        console.log(ResourceList[key] + " - Menge: " + amount + " | Verbrauch: " + (consumption * 100).toFixed(2) + "% | Grünmenge: " + Math.round(greenAmount) + " | Fehlmenge: " + Math.round(greenAmount - amount) + " | Gefahren: " + (amount - storage.LastAmount) + "(" + (tonsMinute / limit * 100).toFixed(2) + "%)" + " (~" + Math.round(tonsMinute) + "/min, Net: " + Math.round(netTonsMinuteCur) + "/min), Net Grün: " + Math.round(netTonsMinute) + "/min)  | Menge bis Be: ~" + Math.round(endTon) + "(" + ((endTon / limit) * 100).toFixed(1) + "%) | Fehlmenge Be: ~" + Math.round(greenAmount - endTon) + " | Maximal Abzug: " + Math.round(greenAmount * consumption) + "| Aktueller Abzug: " + Math.round(amount * consumption) + " | Min bis Grün: " + ((netTonsMinute < 0 && endTon <= greenAmount ? 9999 : timetoGreen)).toFixed(1) + "min");
                        $('#datatable').append(innerTable);

                        // Pastdata log
                        if (typeof pastData[townHash] != 'undefined') {
                            var storage60 = data60.StoragesInfo.Storages.filter(stor => stor.ResourceId == key)[0]
                            var storage180 = data180.StoragesInfo.Storages.filter(stor => stor.ResourceId == key)[0]
                            var storage300 = data300.StoragesInfo.Storages.filter(stor => stor.ResourceId == key)[0]
                            var amount60 = storage60.Amount;
                            var amount180 = storage180.Amount;
                            var amount300 = storage300.Amount;
                            var tons60 = amount - amount60;
                            var tons180 = amount - amount180;
                            var tons300 = amount - amount300;
                            var seconds60 = (Date.now() - parseInt(key60)) / 1000;
                            var seconds120 = (Date.now() - parseInt(key180)) / 1000;
                            var seconds180 = (Date.now() - parseInt(key300)) / 1000;
                            var tonsMinute60 = (tons60 / seconds60) * 60;
                            var tonsMinute180 = (tons180 / seconds120) * 60;
                            var tonsMinute300 = (tons300 / seconds180) * 60;
                            var endTon60 = amount + ((900 - secondsCalc) / 60 * tonsMinute60)
                            var endTon180 = amount + ((900 - secondsCalc) / 60 * tonsMinute180)
                            var endTon300 = amount + ((900 - secondsCalc) / 60 * tonsMinute300)

                            innerTable2.append('<tr><td>' + ResourceList[key] + '</td><td>' + new Intl.NumberFormat('de-DE').format(Math.round(tonsMinute60)) + ' (' + (tonsMinute60 / limit * 100).toFixed(2) + '%)</td><td>' + new Intl.NumberFormat('de-DE').format(Math.round(greenAmount - endTon60)) + '</td><td>' + new Intl.NumberFormat('de-DE').format(Math.round(tonsMinute180)) + ' (' + (tonsMinute180 / limit * 100).toFixed(2) + '%)</td><td>' + new Intl.NumberFormat('de-DE').format(Math.round(greenAmount - endTon180)) + '</td><td>' + new Intl.NumberFormat('de-DE').format(Math.round(tonsMinute300)) + ' (' + (tonsMinute300 / limit * 100).toFixed(2) + '%)</td><td>' + new Intl.NumberFormat('de-DE').format(Math.round(greenAmount - endTon300)) + '</td></tr>')
                            console.log(ResourceList[key] + " - Schnitt 1min: " + Math.round(tonsMinute60) + "/min (" + (tonsMinute60 / limit * 100).toFixed(2) + "%) | Schnitt 3min: " + Math.round(tonsMinute180) + "/min (" + (tonsMinute180 / limit * 100).toFixed(2) + "%) | Schnitt 5min: " + Math.round(tonsMinute300) + "/min (" + (tonsMinute300 / limit * 100).toFixed(2) + "%)");

                        }
                    }

                }
                $('#datatable').append(innerTable2);
                lastcalc = secondsCalc;
                if (typeof pastData[townHash] == 'undefined') {
                    pastData[townHash] = {};
                }
                pastData[townHash][time] = data.Body;


                console.log(pastData[townHash]);
            }

            if (url.includes("interface=SightInterface&method=GetOverview") && this.responseText != "" && showLM) {
                var data = JSON.parse(this.responseText);
                var stockList = data.Body.Stock;

                console.log("*** WHZ ***");
                var timeAll = 0;
                var countAll = 0;
                var toMinAll = 0;
                var toRemainAll = 0;
                for (var skey in stockList) {
                    var stock = stockList[skey];
                    var amount = stock.Amount;
                    var limit = stock.Limit;
                    var trend = stock.Trend;
                    var toMin = (limit / 100 * trend) / 15;
                    var remainTo = limit - amount;
                    var percentRemain = (1 - (amount / limit)) * 100;
                    var timeRemain = percentRemain / trend / 4;
                    if (percentRemain > 0) {
                        if (stock.ResourceId != 50 && stock.ResourceId != 49) {
                            timeAll = timeAll + timeRemain;
                            toRemainAll = toRemainAll + remainTo;
                            toMinAll = toMinAll + toMin;
                            countAll++;
                        }
                        console.log(ResourceList[stock.ResourceId] + " - Menge: " + amount + " | Limit: " + limit + " | Trend: " + trend.toFixed(2) + "% | Zeit bis Grün: " + timeRemain.toFixed(1) + "h");
                    }
                }

                console.log("Gesamtzeit: " + (timeAll / countAll).toFixed(1) + "h");
                console.log("Gesamtzeit tomin: " + (toRemainAll / toMinAll / 60).toFixed(1) + "h");

            }
        }, false);

        open.call(this, method, url, async, user, pass);
    };


})(XMLHttpRequest.prototype.open);
