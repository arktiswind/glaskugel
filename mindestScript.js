// ==UserScript==
// @name         Glaskugel
// @namespace    https://gitlab.com/arktiswind/glaskugel/
// @version      0.2
// @description  try to take over RN!
// @author       Arktiswind
// @match        <$URL$>
// @grant        GM.xmlHttpRequest
// ==/UserScript==

(function (open) {
    'use strict';

    XMLHttpRequest.prototype.wrappedSend = XMLHttpRequest.prototype.send;

    Object.filterTime = (obj, time) =>
        Object.keys(obj)
            .filter(key => key < Date.now() - (time * 1000))
            .reduce((res, key) => (res[key] = obj[key], res), {});

    // Override the existing setRequestHeader function so that it stores the headers
    XMLHttpRequest.prototype.send = function (value) {
        // Call the wrappedSetRequestHeader function first
        // so we get exceptions if we are in an erronous state etc.
        this.wrappedSend(value);
        this.payload = value;
    }

    XMLHttpRequest.prototype.open = function (method, url, async, user, pass) {

        this.addEventListener("readystatechange", function () {

            // ab hier gehts los
            // zB getTownDetails als Name
            if (url.includes("TEILS DES NAMENS DER FLASH") && this.responseText != "") {
                var data = JSON.parse(this.responseText);
                console.log(data);
            }

        }, false);

        open.call(this, method, url, async, user, pass);
    }
})(XMLHttpRequest.prototype.open);