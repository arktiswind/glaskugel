# Glaskugel

To use with tampermonkey in chromium based browsers.
https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=de

## Install

- Copy code in userscript
- Set config:
![Alt text](image.png)
